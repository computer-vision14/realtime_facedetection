
import cv2
import numpy as np
import os
from datetime import datetime

cam = cv2.VideoCapture (0)

face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")


if not os.path.exists('detections'):
    os.mkdir('detections') # make sure you have a detections folder

    
while True:    
    _, frame = cam.read()    
    
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)    
    
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)    
    
    for (x, y, width, height) in faces:    
        cv2.rectangle(frame, (x, y), (x + width, y + height), (255, 0, 0), 3)
        face_roi = frame[y:y+height, x:x+width]


        if not os.path.exists('detections/' + datetime.now().strftime('%Y-%m-%d')):
            os.mkdir('detections/' + datetime.now().strftime("%Y-%m-%d"))

        cv2.imwrite('detections/' + datetime.now().strftime("%Y-%m-%d") + '/' + datetime.now().strftime("%H-%M") + '.jpg', face_roi)

    cv2.imshow('frame', frame)
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break